# Battery Health ([source Oscar Liang blog](https://oscarliang.com/when-retire-lipo-battery/))

Health indicator: Internals Resistance (IR)

IR can be meassured e.g. with charger and is lower, when parallel charging batteries.

Rough IR ranges for typcial `1300 - 1500 mAh`:
* great condition: `IR < 10 mOhm`
* fine condition: `10 mOhm < IR < 15 mOhm`
* old: `15 mOhm < IR < 20 mOhm`
* retire condition: `20 mOhm < IR`

### When to replace battery

Expected LiPo cycles (when handled with care): `400-500`

Replacement typically after 2-3 years of intensive use

* internal resistance is too high
* internal resistance of once cell is much higher than other cells
* battery is puffed
* battery is getting much hotter than newer batteries both after flight and during charging

# PID Tuning Guides

* [Betaflight](https://github.com/betaflight/betaflight/wiki/PID-Tuning-Guide)
* [Rotorsports](https://rotorsports.io/PID_Tuning)
* [in-flight tuning](https://www.propwashed.com/in-flight-adjustments/) + [Video](https://youtu.be/Qsgu2Q-PhSQ)
* [Multiwii](http://www.multiwii.com/wiki/index.php?title=PID#Basic_PID_Tuning)
* [Arducopter](https://diydrones.com/forum/topics/arducopter-tuning-guide)

# Achieving Linear Throttle Response

[Oscar Liang](https://oscarliang.com/linear-throttle-response-motor-top-end-calibration/)

# Taranis Logical Switches for Reliable Voltage Alarm

[Oscar Liang](https://oscarliang.com/taranis-logical-switches-voltage-alarm/)

# Update D4R-II Receiver Firmware

[Oscar Liang](https://oscarliang.com/upgrade-frsky-rx-firmware-d4r-ii-x4r-sb/)

